'''
This is a simple python3 program to verify that all the LEDs on the Raspberry Pi - Z80 Controller V1.1 PCB light up correctly.

Note: if some of the LED's don't light up correctly, you might have a solder issue on a LED or resistor pin.  This will cause the Z80 to not work correctly!

With the power off, remove the Z80 CPU, power on the RPi, then run this on the Raspberry Pi via:  python3 test-leds.py

Parkview 2021-05-13 (cc)
'''

import RPi.GPIO as GPIO
import time
import board
import busio
import time
import curses
import os
import atexit
import array
import sys
from datetime import datetime
from adafruit_mcp230xx.mcp23017 import MCP23017

# system setup
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
# setup RPi direct pins
GPIO.setup(16,GPIO.OUT)
GPIO.setup(17,GPIO.OUT)
GPIO.setup(18,GPIO.OUT)

def rpiLeds_On():
  print("RPi LEDs on")
  GPIO.output(16,GPIO.HIGH)
  GPIO.output(17,GPIO.HIGH)
  GPIO.output(18,GPIO.HIGH)

def rpiLeds_Off():
  print("RPi LEDs off")
  GPIO.output(16,GPIO.LOW)
  GPIO.output(17,GPIO.LOW)
  GPIO.output(18,GPIO.LOW)


#setup i2c boards
li2c = busio.I2C(board.SCL, board.SDA)
ri2c = busio.I2C(board.SCL, board.SDA)
bi2c = busio.I2C(board.SCL, board.SDA)
left = MCP23017(li2c, address=0x20)
right = MCP23017(ri2c, address=0x21)
bottom = MCP23017(bi2c, address=0x22)


#  Address bus MCP23017 - 0x20
a0 = left.get_pin(0)
a1 = left.get_pin(1)
a2 = left.get_pin(2)
a3 = left.get_pin(3)
a4 = left.get_pin(4)
a5 = left.get_pin(5)
a6 = left.get_pin(6)
a7 = left.get_pin(7)
a8 = left.get_pin(8)
a9 = left.get_pin(9)
a10 = left.get_pin(10)
a11 = left.get_pin(11)
a12 = left.get_pin(12)
a13 = left.get_pin(13)
a14 = left.get_pin(14)
a15 = left.get_pin(15)

# Data bus MCP2317 - 0x21
d0 = right.get_pin(0)
d1 = right.get_pin(1)
d2 = right.get_pin(2)
d3 = right.get_pin(3)
d4 = right.get_pin(4)
d5 = right.get_pin(5)
d6 = right.get_pin(6)
d7 = right.get_pin(7)

# Z80 IO MCP23017 - 0x22
clockpin = bottom.get_pin(0)
rdpin = bottom.get_pin(1)
wrpin = bottom.get_pin(2)
reset = bottom.get_pin(3)
power = bottom.get_pin(15)

#set address lines to read
a0.switch_to_output()
a1.switch_to_output()
a2.switch_to_output()
a3.switch_to_output()
a4.switch_to_output()
a5.switch_to_output()
a6.switch_to_output()
a7.switch_to_output()
a8.switch_to_output()
a9.switch_to_output()
a10.switch_to_output()
a11.switch_to_output()
a12.switch_to_output()
a13.switch_to_output()
a14.switch_to_output()
a15.switch_to_output()

d0.switch_to_output(value=False)
d1.switch_to_output(value=False)
d2.switch_to_output(value=False)
d3.switch_to_output(value=False)
d4.switch_to_output(value=False)
d5.switch_to_output(value=False)
d6.switch_to_output(value=False)
d7.switch_to_output(value=False)

clockpin.switch_to_output(value=False)
rdpin.switch_to_output(value=False)
wrpin.switch_to_output(value=False)
reset.switch_to_output(value=False)
power.switch_to_output(value=False)

clockpin.value = True
power.value = True
reset.value = True
rdpin.value = True
wrpin.value = True
a0.value = True
a1.value = True
a2.value = True
a3.value = True
a4.value = True
a5.value = True
a6.value = True
a7.value = True
a8.value = True
a9.value = True
a10.value = True
a11.value = True
a12.value = True
a13.value = True
a14.value = True
a15.value = True
d1.value = True
d2.value = True
d3.value = True
d4.value = True
d5.value = True
d6.value = True
d7.value = True
d0.value = True
time.sleep(5)
clockpin.value = False
power.value = False
reset.value = False 
rdpin.value = False
wrpin.value = False
a0.value = False
a1.value = False
a2.value = False
a3.value = False
a4.value = False
a5.value = False
a6.value = False
a7.value = False
a8.value = False
a9.value = False
a10.value = False
a11.value = False
a12.value = False
a13.value = False
a14.value = False
a15.value = False
d1.value = False
d2.value = False
d3.value = False
d4.value = False
d5.value = False
d6.value = False
d7.value = False
d0.value = False

# test the RPi directly connected LEDs
rpiLeds_On()
time.sleep(1)
rpiLeds_Off()
time.sleep(1)
rpiLeds_On()
time.sleep(1)
rpiLeds_Off()
time.sleep(1)
