;  LED-Test.asm
;
;  This Z80 code is a method of testing each of the data and address bus LEDs
;
;  Compile via:  bin/vasmz80_oldstyle -Fbin -dotdir led-test.asm -o led-test.com
;  copy to z80 run directory: cp led-test.com ../../z80/
;
; Parkview 2021-03-31





 	.org 0000 
rept:	.equ	0020h   ; how many times to write to adress - so that we can see the correct address LED
ram:	.equ	0040h   ; start at ram 64D address, then jump by shifting 1 bit.
start:
 	ld a,1
 	ld bc,ram  ; BC=first RAM slot to write too
 	ld hl,rept ; HL= how many times to repeat the memory write.  It has to be enough so that we see it
 	ld de,$1   ; subtract from HL, so we get a flag showing
loop: 
 	ld (bc),a
 	;dec bc
 	sbc hl,de
 	jp nz,loop
 	ld hl,rept
 	rrca
 	rl c     ; rotate C right by one bit
 	jr nc,skiph
 	rl b     ; rotate B right by one bit
 	jr nc,skiph
 	ld bc,ram  ; we have reached the end, so lets start again
skiph:
 	ld hl,rept  ; make 100 a constant!
 	jp loop
