'''

The majority of this file was written by:  James Fitzjohn - more information about this project can be found here:  
https://www.hackster.io/james-fitzjohn/raspberry-pi-to-z80-interface-0bfbeb#toc-interface-software-6

Notes:
------ 

* to run this python3 cursor based program, you will need a Raspberry Pi terminal window with at least 77 lines available.  I managed this by editing ?? file and adding:  ??
  rebooting the RPi, and then from the desktop GUI, pressing the  CTL + ALT + F1  keys to toggle over to CLI mode.  Use:  ALT + F7   to toggle back to the GUI.

* currently the PCB buzzer and OLED screen are not being utilised
	  
	  
To run this program:  
--------------------

python3 cpu2.py    #  runs a default built in test program

python3 cpu2.py led-test.com   # loads a compiled Z80 test program into pseudo ram and the Z80 runs the program


While the program is running you can use the following keys:   

q = quit
r = reset  # must be held down for a while before activating
p = pause
d = debug  # display a line of debug information
z = disable Z80   # turn off the Z80, but keep the emulation going
m = save current RAM  to file:  ram.txt

right-arrow = go forward 1000 bytes in the display
left-arrow = go back 1000 bytes in the display
up-arrow = go back 16 bytes in the display
down-arrow = go forward 16 bytes in the display




'''

import board
import busio
import time
import curses
import os
import atexit
import array
#import cProfile
#import I2C_LCD_driver
import sys
from datetime import datetime
from adafruit_mcp230xx.mcp23017 import MCP23017
from z80 import z80

#create emulated cpu
cpu = z80()

#setup output
screen = curses.initscr()
curses.noecho()
screen.nodelay(1)
screen.keypad(1)
curses.start_color()
curses.init_pair(1,curses.COLOR_YELLOW,curses.COLOR_BLACK)
curses.init_pair(2,curses.COLOR_GREEN,curses.COLOR_BLACK)
curses.init_pair(3,curses.COLOR_RED,curses.COLOR_BLACK)
#mylcd = I2C_LCD_driver.lcd()

#**************************a few functions to make the code easier to read*******************************
def opcodetoram(code):
    bitstring = "{0:08b}".format(int(code,16))
    BYTE = [bool(int(bitstring[7])),bool(int(bitstring[6])),bool(int(bitstring[5])),bool(int(bitstring[4])),bool(int(bitstring[3])),bool(int(bitstring[2])),bool(int(bitstring[1])),bool(int(bitstring[0]))]
    return BYTE

def bool8str(d7,d6,d5,d4,d3,d2,d1,d0): #checking each bit is a little faster than + ing string or .join ing them
   # line = "".join([str(int(d7)) , str(int(d6)) , str(int(d5)) , str(int(d4)) , str(int(d3)) , str(int(d2)) , str(int(d1)) , str(int(d0))])
    line = ""
    if d7 == True: line = line + "1"
    else: line = line + "0"
    if d6 == True: line = line + "1"
    else: line = line + "0"
    if d5 == True: line = line + "1"
    else: line = line + "0"
    if d4 == True: line = line + "1"
    else: line = line + "0"
    if d3 == True: line = line + "1"
    else: line = line + "0"
    if d2 == True: line = line + "1"
    else: line = line + "0"
    if d1 == True: line = line + "1"
    else: line = line + "0"
    if d0 == True: line = line + "1"
    else: line = line + "0"
    return line

def bool16str(a15,a14,a13,a12,a11,a10,a9,a8,a7,a6,a5,a4,a3,a2,a1,a0):
    #line = "".join([str(int(a15)) , str(int(a14)) , str(int(a13)) , str(int(a12)) , str(int(a11)) , str(int(a10)) , str(int(a9)) , str(int(a8)) , str(int(a7)) , str(int(a6)) , str(int(a5)) , str(int(a4)) , str(int(a3)) , str(int(a2)) , str(int(a1)) , str(int(a0))])
    line = ""
    if a15 == True: line = line + "1"
    else: line = line + "0"
    if a14 == True: line = line + "1"
    else: line = line + "0"
    if a13 == True: line = line + "1"
    else: line = line + "0"
    if a12 == True: line = line + "1"
    else: line = line + "0"
    if a11 == True: line = line + "1"
    else: line = line + "0"
    if a10 == True: line = line + "1"
    else: line = line + "0"
    if a9 == True: line = line + "1"
    else: line = line + "0"
    if a8 == True: line = line + "1"
    else: line = line + "0"
    if a7 == True: line = line + "1"
    else: line = line + "0"
    if a6 == True: line = line + "1"
    else: line = line + "0"
    if a5 == True: line = line + "1"
    else: line = line + "0"
    if a4 == True: line = line + "1"
    else: line = line + "0"
    if a3 == True: line = line + "1"
    else: line = line + "0"
    if a2 == True: line = line + "1"
    else: line = line + "0"
    if a1 == True: line = line + "1"
    else: line = line + "0"
    if a0 == True: line = line + "1"
    else: line = line + "0"
    return line

def notoascii(number):
    letter = ""
   
    if number == 9: letter = "\t"
    #if number == 10: letter = "\r"
    if number == 13: letter = "\n"
    if number == 32: letter = " "
    if number == 33: letter = "!"
    if number == 34: letter = "\""
    if number == 35: letter = "#"
    if number == 36: letter = "$"
    if number == 37: letter = "%"
    if number == 38: letter = "&"
    if number == 39: letter = "'"
    if number == 40: letter = "("
    if number == 41: letter = ")"
    if number == 42: letter = "*"
    if number == 43: letter = "+"
    if number == 44: letter = ","
    if number == 45: letter = "-"
    if number == 46: letter = "."
    if number == 47: letter = "/"
    if number == 48: letter = "0"
    if number == 49: letter = "1"
    if number == 50: letter = "2"
    if number == 51: letter = "3"
    if number == 52: letter = "4"
    if number == 53: letter = "5"
    if number == 54: letter = "6"
    if number == 55: letter = "7"
    if number == 56: letter = "8"
    if number == 57: letter = "9"
    if number == 58: letter = ":"
    if number == 59: letter = ";"
    if number == 60: letter = "<"
    if number == 61: letter = "="
    if number == 62: letter = ">"
    if number == 63: letter = "?"
    if number == 64: letter = "@"
    if number == 65: letter = "A"
    if number == 66: letter = "B"
    if number == 67: letter = "C"
    if number == 68: letter = "D"
    if number == 69: letter = "E"
    if number == 70: letter = "F"
    if number == 71: letter = "G"
    if number == 72: letter = "H"
    if number == 73: letter = "I"
    if number == 74: letter = "J"
    if number == 75: letter = "K"
    if number == 76: letter = "L"
    if number == 77: letter = "M"
    if number == 78: letter = "N"
    if number == 79: letter = "O"
    if number == 80: letter = "P"
    if number == 81: letter = "Q"
    if number == 82: letter = "R"
    if number == 83: letter = "S"
    if number == 84: letter = "T"
    if number == 85: letter = "U"
    if number == 86: letter = "V"
    if number == 87: letter = "W"
    if number == 88: letter = "X"
    if number == 89: letter = "Y"
    if number == 90: letter = "Z"
    if number == 91: letter = "["
    if number == 92: letter = "\\"
    if number == 93: letter = "]"
    if number == 94: letter = "^"
    if number == 95: letter = "_"
    if number == 96: letter = "`"
    if number == 97: letter = "a"
    if number == 98: letter = "b"
    if number == 99: letter = "c"
    if number == 100: letter = "d"
    if number == 101: letter = "e"
    if number == 102: letter = "f"
    if number == 103: letter = "g"
    if number == 104: letter = "h"
    if number == 105: letter = "i"
    if number == 106: letter = "j"
    if number == 107: letter = "k"
    if number == 108: letter = "l"
    if number == 109: letter = "m"
    if number == 110: letter = "n"
    if number == 111: letter = "o"
    if number == 112: letter = "p"
    if number == 113: letter = "q"
    if number == 114: letter = "r"
    if number == 115: letter = "s"
    if number == 116: letter = "t"
    if number == 117: letter = "u"
    if number == 118: letter = "v"
    if number == 119: letter = "w"
    if number == 120: letter = "x"
    if number == 121: letter = "y"
    if number == 122: letter = "z"
    if number == 123: letter = "{"
    if number == 124: letter = "|"
    if number == 125: letter = "}"
    if number == 126: letter = "~"
    return letter


#************************************setup ram**********************************************************
pRAM = []
eRAM = []


def resetram():
    x = 0
    numberofbytes = 65536
    while x < numberofbytes:
        pRAM.append([False,False,False,False,False,False,False,False])
        eRAM.append([False,False,False,False,False,False,False,False])
        x = x + 1

   
    #if we have an input file read it into RAM else load a smaple program
    filename = ""
    address = 0
    if sys.argv[1:]:
        filename = sys.argv[1]
    
        file = open(filename,"rb")
        byte = file.read(1)
        while byte:
            number = int.from_bytes(byte,byteorder='big')
            line = format(int(number),'02x')
            line = line.upper()
            pRAM[address] = opcodetoram(line)
            eRAM[address] = opcodetoram(line)
            address = address + 1
            byte = file.read(1)
    
        file.close()
    else:
        
        pRAM[0] = opcodetoram("01")
        pRAM[1] = opcodetoram("bb")
        pRAM[2] = opcodetoram("cc")
        
        pRAM[3] = opcodetoram("dd")
        pRAM[4] = opcodetoram("41")
        pRAM[5] = opcodetoram("c5")
        pRAM[6] = opcodetoram("76")
   
     #   pRAM[7] = opcodetoram("36")
     #   pRAM[8] = opcodetoram("46")
     #   pRAM[9] = opcodetoram("36")
     #   pRAM[10] = opcodetoram("41")# LD HL 1388
     #   pRAM[11] = opcodetoram("36") 
     #   pRAM[12] = opcodetoram("49") 
     #   pRAM[13] = opcodetoram("36")
     #   pRAM[14] = opcodetoram("4c")
   #     pRAM[9] = opcodetoram("76")

#        pRAM[256] = opcodetoram("90")
#        pRAM[257] = opcodetoram("48")




     

    #point at which to panic
   # panicpoint = 7286 
   # pRAM[panicpoint] = opcodetoram("C3")
   # pRAM[panicpoint + 1] = opcodetoram("58")
   # pRAM[panicpoint + 2] = opcodetoram("1B")

#    pRAM[7653] = opcodetoram("C3")
#    pRAM[7654] = opcodetoram("10")
#    pRAM[7655] = opcodetoram("27")


    #panic routine, dump registers and halt
     
#    pRAM[10000] = opcodetoram("31") # set SP back to top
#    pRAM[10001] = opcodetoram("FF")
#    pRAM[10002] = opcodetoram("FF")

 #   pRAM[10003] = opcodetoram("F5") # Push Af 
 #   pRAM[10004] = opcodetoram("C5") # push BC
 #   pRAM[10005] = opcodetoram("D5") # Push DE
 #   pRAM[10006] = opcodetoram("E5") # Push HL
 #   pRAM[10007] = opcodetoram("76") # Halt
    
#    pRAM[0] = opcodetoram("01")
#    pRAM[1] = opcodetoram("FF")
 #   pRAM[2] = opcodetoram("FF")

  #  pRAM[3] = opcodetoram("11")
 #   pRAM[4] = opcodetoram("FF")
 #   pRAM[5] = opcodetoram("FF")

  #  pRAM[6] = opcodetoram("21")
  #  pRAM[7] = opcodetoram("FF")
#    pRAM[39000] = opcodetoram("FF")




    x = 0
    numberofbytes = 65535 
    while x < numberofbytes:
        eRAM[x] = pRAM[x]
        x = x + 1

resetram()
   
#******************************************setup the hardware and variables*********************************

#setup i2c boards
li2c = busio.I2C(board.SCL, board.SDA)
ri2c = busio.I2C(board.SCL, board.SDA)
bi2c = busio.I2C(board.SCL, board.SDA)
left = MCP23017(li2c, address=0x20)
right = MCP23017(ri2c, address=0x21)
bottom = MCP23017(bi2c, address=0x22)


#the left i2c GPIO extender handles the address bus
a0 = left.get_pin(0)
a1 = left.get_pin(1)
a2 = left.get_pin(2)
a3 = left.get_pin(3)
a4 = left.get_pin(4)
a5 = left.get_pin(5)
a6 = left.get_pin(6)
a7 = left.get_pin(7)
a8 = left.get_pin(8)
a9 = left.get_pin(9)
a10 = left.get_pin(10)
a11 = left.get_pin(11)
a12 = left.get_pin(12)
a13 = left.get_pin(13)
a14 = left.get_pin(14)
a15 = left.get_pin(15)

#The right i2c GPIO extender handles the data bus
d0 = right.get_pin(0)
d1 = right.get_pin(1)
d2 = right.get_pin(2)
d3 = right.get_pin(3)
d4 = right.get_pin(4)
d5 = right.get_pin(5)
d6 = right.get_pin(6)
d7 = right.get_pin(7)

#The bottom i2c GPIO extender handles the other IO
clockpin = bottom.get_pin(0)
rdpin = bottom.get_pin(1)
wrpin = bottom.get_pin(2)
reset = bottom.get_pin(3)
power = bottom.get_pin(15)
power.switch_to_output(value=True)

#set address lines to read
a0.switch_to_input()
a1.switch_to_input()
a2.switch_to_input()
a3.switch_to_input()
a4.switch_to_input()
a5.switch_to_input()
a6.switch_to_input()
a7.switch_to_input()
a8.switch_to_input()
a9.switch_to_input()
a10.switch_to_input()
a11.switch_to_input()
a12.switch_to_input()
a13.switch_to_input()
a14.switch_to_input()
a15.switch_to_input()

d0.switch_to_output(value=False)
d1.switch_to_output(value=False)
d2.switch_to_output(value=False)
d3.switch_to_output(value=False)
d4.switch_to_output(value=False)
d5.switch_to_output(value=False)
d6.switch_to_output(value=False)
d7.switch_to_output(value=False)

clockpin.switch_to_output(value=True)
rdpin.switch_to_input()
wrpin.switch_to_input()
reset.switch_to_output(value=True)


clock = True
ticks = 0
pause = False
ticklen = 0.001
running = True
plastaddrline = ""
elastaddrline = ""
debugline = ""
memaddr = 0
viewoffset = 1000
cleanreset = False
syncdelay = 4
reset.value = False 
resetticks = 0
oldflagline = ""
olddebugline = ""
poldoutputchar = ""
pConsoleline = []
pConsoleline.append("")
pConsoleline.append("")
pConsoleline.append("")
pConsoleline.append("")
pConsoleline.append("")
eoldoutputchar = ""
eConsoleline = []
eConsoleline.append("")
eConsoleline.append("")
eConsoleline.append("")
eConsoleline.append("")
eConsoleline.append("")
logfile = open("log.txt","w")
logfile.write("P-PC\tMatch\tE-PC\tE-PC hex\tOpcode\t\tOpc (h)\tWrite\tRead\tInstruction\n")
tic = ""
toc = ""
freq = "0"
ticcounter = 0
pConsoleno = 0
eConsoleno = 0
frame = 0
rframe = 10 
direction = ""
physicalcpu = True 
pwr = True
prd = True
halt = False
startTime = datetime.now()
endTime = "Running"

if sys.argv[1:]:
    frame = 16
    rframe = 20 

else:
    ticklen = 0.1
    rframe = 1
    viewoffset = 0


while resetticks < 5:
    resetticks = resetticks + 1
    clockpin.value = False
    time.sleep(ticklen)
    clockpin.value = True
    time.sleep(ticklen)
reset.value = True

while running == True:

        #*************************************dont render EVERY clock tick*******************************
        frame = frame + 1        
        if frame == rframe: screen.erase()

        #***********************************quick frequency hz counter***********************************
        if tic == "":
            tic = time.perf_counter()
        ticcounter = ticcounter + 1

        if ticcounter == rframe:
            toc = time.perf_counter()
            ticcounter = 0
            freq =  str((1/( (toc - tic) / rframe )))
            freq = str(freq.partition('.')[0])
            tic = ""

        #******************check if were paused, if not alternate the clock pin***************************
        if pause == False:
            ticks = ticks + 1
            if physicalcpu == True: bottom.gpio = bottom.gpio + 1 
        
        #**********************************Title**********************************************************
        line = "Z80 Emulation comparison"
        if sys.argv[1:]:
            if sys.argv[1] != "": 
                line = line + " (" + sys.argv[1] + ") (" + endTime + ")" 
           
                if halt == False:
                    if cpu.instructionname == "HALT":
                        halt = True
                        end = datetime.now() - startTime
                        endTime = "Complete " + str(end) + " seconds"

        if frame == rframe: 
            screen.addstr(0,50,line)
            screen.addstr(2,50,"Total clock ticks = " + str(ticks) + " " + freq + "hz")
            screen.addstr(4,50,"**Traditionally Emuldated Z80**",curses.color_pair(2))
 

        #********************************check the read write pins ********************************************  
        if physicalcpu == True:
            bot = format(bottom.gpio,'016b')
            if bot[14] == "1": prd = 1
            else: prd = 0
            if bot[13] == "1": pwr = 1
            else: pwr = 0

            if frame == rframe: screen.addstr(5,0,"Read = " + str(prd) + " Write = " + str(pwr))
            x = left.gpio
            pbinaddr = format(x,'016b')
            phexaddr = format(int(pbinaddr,2),'04X')
           # memaddr = x

            #debugline = "X = " + str(x) + " left gpio = " + str(left.gpio)
            if frame == rframe: screen.addstr(4,0,"**Physical Z80**",curses.color_pair(1))
 
 
        erd = int(cpu.RD)
        ewr = int(cpu.WR)
        if frame == rframe: screen.addstr(5,50,"Read = " + str(erd) + " Write = " + str(ewr))
        ebinaddr = bool16str(cpu.a15,cpu.a14,cpu.a13,cpu.a12,cpu.a11,cpu.a10,cpu.a9,cpu.a8,cpu.a7,cpu.a6,cpu.a5,cpu.a4,cpu.a3,cpu.a2,cpu.a1,cpu.a0)
        ehexaddr = format(int(ebinaddr,2),'04X')
        y = int(ehexaddr,16)

        
        
        if pwr == False and physicalcpu == True:#write contents of data bus to address in RAM
            if direction != "input":
                right.iodir = 65535
            direction = "input"
            databus = format(right.gpio,'08b')
            pRAM[x] = [bool(int(databus[7])),bool(int(databus[6])),bool(int(databus[5])),bool(int(databus[4])),bool(int(databus[3])),bool(int(databus[2])),bool(int(databus[1])),bool(int(databus[0]))]
            memaddr = x

        if prd == False and physicalcpu == True:
            if direction != "output": 
                right.iodir = 65280
            memaddr = x
            right.gpio = int(bool8str(pRAM[x][7],pRAM[x][6],pRAM[x][5],pRAM[x][4],pRAM[x][3],pRAM[x][2],pRAM[x][1],pRAM[x][0]),2)
            direction = "output"

        
        if ewr == False:
            data = bool8str(cpu.d7,cpu.d6,cpu.d5,cpu.d4,cpu.d3,cpu.d2,cpu.d1,cpu.d0)
            eRAM[y] = [cpu.d0,cpu.d1,cpu.d2,cpu.d3,cpu.d4,cpu.d5,cpu.d6,cpu.d7]

         
        if erd == False:
            cpu.d0 = eRAM[y][0]
            cpu.d1 = eRAM[y][1]
            cpu.d2 = eRAM[y][2]
            cpu.d3 = eRAM[y][3]
            cpu.d4 = eRAM[y][4]
            cpu.d5 = eRAM[y][5]
            cpu.d6 = eRAM[y][6]
            cpu.d7 = eRAM[y][7]

#we have to do the emulated cpu clock after setting the pins else we would read the previous value, the physical cpu needs time for its pins to calm down and we dont know its PC until we have read the address is wants
        if pause == False and syncdelay == 0:
            cpu.clock(True)
        if syncdelay > 0: syncdelay = syncdelay - 1



        #****************************************Whats on the busses**************************************
        #physical Z80
        if physicalcpu == True and frame == rframe:
            bindata = bool8str(pRAM[memaddr][7],pRAM[memaddr][6],pRAM[memaddr][5],pRAM[memaddr][4],pRAM[memaddr][3],pRAM[memaddr][2],pRAM[memaddr][1],pRAM[memaddr][0])
            hexdata = int(bindata,2)
            hexdata = format(hexdata,'02X')
            hexdata.rjust(4,'0')
            screen.addstr(6,0,"Data bus:" + bindata + " (" + hexdata + ")")
            if prd == False or pwr == False:#wait until the cpu is ready
                plastaddrline = "Address bus:" + pbinaddr + " (" + phexaddr + ")"               
            screen.addstr(7,0,plastaddrline)

       
        #Emulated Z80
        if frame == rframe:
            bindata = bool8str(cpu.d7,cpu.d6,cpu.d5,cpu.d4,cpu.d3,cpu.d2,cpu.d1,cpu.d0)
            hexdata = int(bindata,2)
            hexdata = format(hexdata,'02X')
            hexdata.rjust(4,'0')
            screen.addstr(6,50,"Data bus:" + bindata + " (" + hexdata + ")")
            if erd == False or ewr == False:#we until the emulated cpu is ready
                elastaddrline =  "Address bus:" + bool16str(cpu.a15,cpu.a14,cpu.a13,cpu.a12,cpu.a11,cpu.a10,cpu.a9,cpu.a8,cpu.a7,cpu.a6,cpu.a5,cpu.a4,cpu.a3,cpu.a2,cpu.a1,cpu.a0) + " (" + ehexaddr + ")"      
            screen.addstr(7,50,elastaddrline)




        #********************************************Show RAM********************************************

        if frame == rframe:
            if physicalcpu == True: screen.addstr(9,0,"Pseudo RAM",curses.color_pair(1))
            screen.addstr(9,50,"Emulator RAM",curses.color_pair(2))
            x = viewoffset 
        
            while x < (viewoffset +16):
                #Physical RAM
                if physicalcpu == True:
                    pramstring = bool8str(pRAM[x][7],pRAM[x][6],pRAM[x][5],pRAM[x][4],pRAM[x][3],pRAM[x][2],pRAM[x][1],pRAM[x][0])
                    phexramstring = format(int(pramstring,2),'02X')
                    pzero_addr = str(x)
                    pzero_addr = pzero_addr.zfill(5)
            
                    if memaddr == x: screen.addstr((10+x)-viewoffset,29,"<- PC",curses.color_pair(1))
                    screen.addstr((10+x)-viewoffset,0,"Address " + pzero_addr + ": " + pramstring + " (" + phexramstring.upper() + ")")


                #emulated RAM
                eramstring = bool8str(eRAM[x][7],eRAM[x][6],eRAM[x][5],eRAM[x][4],eRAM[x][3],eRAM[x][2],eRAM[x][1],eRAM[x][0])
                ehexramstring = format(int(eramstring,2),'02X')
                ezero_addr = str(x)
                ezero_addr = ezero_addr.zfill(5)
              
                if cpu.PC == x: screen.addstr((10+x)-viewoffset,79,"<- PC: " + cpu.instructionname ,curses.color_pair(2))
                screen.addstr((10+x)-viewoffset,50,"Address " + ezero_addr + ": " + eramstring + " (" + ehexramstring.upper() + ")")
                x = x + 1


            
        #**********************************show a few lines of the srack************************************    
        if frame == rframe:
            if physicalcpu == True:
                y = 65535
                linecount = 28
                screen.addstr(27,0,"Pseudo stack",curses.color_pair(1))
                while y > 65526:
                    pramstring = bool8str(pRAM[y][7],pRAM[y][6],pRAM[y][5],pRAM[y][4],pRAM[y][3],pRAM[y][2],pRAM[y][1],pRAM[y][0])
                    phexramstring = format(int(pramstring,2),'02x')
                    screen.addstr(linecount,0,"Address " + str(y) + ": " + pramstring + " (" + phexramstring.upper() + ")")
                    y = y - 1
                    linecount = linecount + 1
         
            y = 65535
            linecount = 28
            if frame == rframe: screen.addstr(27,50,"Emulator stack",curses.color_pair(2))
            while y > 65526:
                eramstring = bool8str(eRAM[y][7],eRAM[y][6],eRAM[y][5],eRAM[y][4],eRAM[y][3],eRAM[y][2],eRAM[y][1],eRAM[y][0])
                ehexramstring = format(int(eramstring,2),'02x')
                screen.addstr(linecount,50,"Address " + str(y) + ": " + eramstring + " (" + ehexramstring.upper() + ")")
                if y == cpu.SP: screen.addstr(linecount,79,"<- SP",curses.color_pair(2))
                y = y - 1
                linecount = linecount + 1


        #***********************************console output****************************************************
        if physicalcpu == True:        
            pcharacter = bool8str(pRAM[32][7],pRAM[32][6],pRAM[32][5],pRAM[32][4],pRAM[32][3],pRAM[32][2],pRAM[32][1],pRAM[32][0])
            if poldoutputchar != pcharacter: 
                poldoutputchar = pcharacter

                if notoascii(int(pcharacter,2)) != "\n": pConsoleline[pConsoleno] = pConsoleline[pConsoleno] + notoascii(int(pcharacter,2))
                else:
                    pConsoleno = pConsoleno + 1
                    if pConsoleno == 5: 
                        pConsoleno = 4

                        pConsoleline[0] = pConsoleline[1]
                        pConsoleline[1] = pConsoleline[2]
                        pConsoleline[2] = pConsoleline[3]
                        pConsoleline[3] = pConsoleline[4]
                        pConsoleline[4] = ""

            if frame == rframe:
                screen.addstr(57,0,"Console output at 20h",curses.color_pair(1))
                screen.addstr(58,0,pConsoleline[0])
                screen.addstr(59,0,pConsoleline[1])
                screen.addstr(60,0,pConsoleline[2])
                screen.addstr(61,0,pConsoleline[3])
                screen.addstr(62,0,pConsoleline[4])
      

        
        echaracter = bool8str(eRAM[32][7],eRAM[32][6],eRAM[32][5],eRAM[32][4],eRAM[32][3],eRAM[32][2],eRAM[32][1],eRAM[32][0])
        
        if eoldoutputchar != echaracter:
            eoldoutputchar = echaracter

            if notoascii(int(echaracter,2)) != "\n": eConsoleline[eConsoleno] = eConsoleline[eConsoleno] + notoascii(int(echaracter,2))
            else:
                eConsoleno = eConsoleno + 1
                if eConsoleno == 5: 
                    eConsoleno = 4

                    eConsoleline[0] = eConsoleline[1]
                    eConsoleline[1] = eConsoleline[2]
                    eConsoleline[2] = eConsoleline[3]
                    eConsoleline[3] = eConsoleline[4]
                    eConsoleline[4] = ""

        if frame == rframe:
            screen.addstr(57,50,"Console output at 20h",curses.color_pair(2))
            screen.addstr(58,50,eConsoleline[0])
            screen.addstr(59,50,eConsoleline[1])
            screen.addstr(60,50,eConsoleline[2])
            screen.addstr(61,50,eConsoleline[3])
            screen.addstr(62,50,eConsoleline[4])
       

 
        #************************************Emulated CPU details****************************************
        lineoffset = 38
        
        if frame == rframe:
            screen.addstr(lineoffset,50,"**Traditionally emulated Z80 details**",curses.color_pair(2))
            screen.addstr(lineoffset + 2,68,"SZ-H-PNC")
            screen.addstr(lineoffset + 3,50,"A = " + cpu.A + ", F = " + cpu.F)
            screen.addstr(lineoffset + 4,50,"BC = " + cpu.B + ":" + cpu.C)
            screen.addstr(lineoffset + 5,50,"DE = " + cpu.D + ":" + cpu.E)
            screen.addstr(lineoffset + 6,50,"HL = " + cpu.H + ":" + cpu.L)
            screen.addstr(lineoffset + 7,50,"IX = " + cpu.IX)
            screen.addstr(lineoffset + 8,50,"IY = " + cpu.IY)

            screen.addstr(lineoffset + 9,50,"SP = " + str(cpu.SP))
            screen.addstr(lineoffset + 10,50,"RD = " + str(erd) + ", WR = " + str(ewr) + " INT = " + str(int(cpu.interupts)))
            screen.addstr(lineoffset + 11,50,"Instruction = ")
            screen.addstr(lineoffset + 11,64, cpu.instructionname, curses.color_pair(2))
            screen.addstr(lineoffset + 12,50,"Instruction delay = " + str(cpu.delay))
            screen.addstr(lineoffset + 13,50,"Need more bits = " + str(cpu.needmorebits))
            screen.addstr(lineoffset + 14,50,"Ready to execute = " + str(cpu.execute))
            screen.addstr(lineoffset + 17,50,"Debug = ")
 
            screen.addstr(lineoffset + 16,50,"Emulated PC = ")
            screen.addstr(lineoffset + 16,64,str(cpu.PC),curses.color_pair(2))
            screen.addstr(lineoffset + 17,58,cpu.debugline, curses.color_pair(2))
       
            if physicalcpu == True:
                screen.addstr(lineoffset + 17,0,"Debug = ")
                screen.addstr(lineoffset + 17,8,debugline, curses.color_pair(1))
                screen.addstr(lineoffset + 16,0,"Physical PC = ")
                screen.addstr(lineoffset + 16,15,str(memaddr),curses.color_pair(1))
                if str(memaddr) != str(cpu.PC): screen.addstr(lineoffset + 16,71,"(Desynced)",curses.color_pair(3))


            screen.addstr(lineoffset + 33,0,"")



        #******************************************record instructions to log.txt***************************** 
        
       
                        
        if cpu.opcode != "":
            allignment = "Good"
            if memaddr != cpu.PC: allignment = "Bad"
            newdebugline = str(memaddr) + "\t" +  allignment + "\t" + str(cpu.PC) + "\t" + format(cpu.PC,'08X') + "\t" + cpu.opcode + "\t" + format(int(cpu.opcode,2),'02X')   + "\t" + str(pwr) + ":" + str(ewr) + "\t" + str(prd) + ":" + str(erd) + "\t" + cpu.instructionname + "\n" 

            logfile.write(newdebugline)
       
        
       
        
       #*********************************************UI control*******************************************
        if frame == rframe: 
           input = screen.getch()
           if input == ord('r') or cleanreset == False: #reset must be active for at least 3 clock ticks
                resetticks = 0
                pRAM.clear()
                eRAM.clear()
                resetram()
                reset.value = False
                while resetticks < 3:
                     resetticks = resetticks + 1
                     clockpin.value = False
                     time.sleep(ticklen)
                     clockpin.value = True
                     time.sleep(ticklen)
                reset.value = True
                cpu.reset() # the emulated cpu doesnt need as much overhead
                cpu.d0 = eRAM[0][0]
                cpu.d1 = eRAM[0][1]
                cpu.d2 = eRAM[0][2]
                cpu.d3 = eRAM[0][3]
                cpu.d4 = eRAM[0][4]
                cpu.d5 = eRAM[0][5]
                cpu.d6 = eRAM[0][6]
                cpu.d7 = eRAM[0][7]
                cleanreset = True
                syncdelay = 4
 
           if input == ord('q'): #quit
                curses.nocbreak()
                curses.echo()
                curses.endwin()
                running = False


           if input == ord('m'): #save physical ram
               ramfile = open("ram.txt","w")

               x = 0
               y = 0

               while x < 65535:
                   
                   pRamString = bool8str(pRAM[x][7],pRAM[x][6],pRAM[x][5],pRAM[x][4],pRAM[x][3],pRAM[x][2],pRAM[x][1],pRAM[x][0]) 
                   ramfile.write(format(int(pRamString,2),'02x')  + "\t")
                   x = x + 1
                   y = y + 1
                   if y == 8: 
                       y = 0;
                       ramfile.write("\r\n")



               ramfile.close()




           if input == ord('z'): #disable the physical CPU
                physicalcpu = False
                rframe = 500

           if input == ord('d'):
                x = 0
                numberofbytes = 65536
                line = ""
                debugline = ""
                while x < numberofbytes:
                    if pRAM[x] != eRAM[x]:
                        debugline = debugline + "Found a differnce at " + str(x) + ", pRAM = " +  str(int(pRAM[x][7])) +  str(int(pRAM[x][6])) +  str(int(pRAM[x][5])) +  str(int(pRAM[x][4])) +  str(int(pRAM[x][3])) +  str(int(pRAM[x][2])) + str(int(pRAM[x][1])) + str(int(pRAM[x][0])) +  " eRAM = " + str(int(eRAM[x][7])) +  str(int(eRAM[x][6])) +  str(int(eRAM[x][5])) +  str(int(eRAM[x][4])) +  str(int(eRAM[x][3])) +  str(int(eRAM[x][2])) +  str(int(eRAM[x][1])) +  str(int(eRAM[x][0])) + " "
                    x = x + 1  


            #quick and easy way to scroll through ram
           if input == curses.KEY_DOWN:
                viewoffset = viewoffset + 16
                if viewoffset > 65535: viewoffset = (65535 - 16)

           if input == curses.KEY_UP:
                 viewoffset = viewoffset - 16
                 if viewoffset < 0: viewoffset = 0

           if input == curses.KEY_RIGHT:
                viewoffset = viewoffset + 1000
                if viewoffset > 65535: viewoffset = (65535 - 16)

           if input == curses.KEY_LEFT:
                viewoffset = viewoffset - 1000
                if viewoffset < 0: viewoffset = 0

        
           if input == ord('p'): pause = not pause
        
        
        if frame == rframe: 
            screen.refresh()
            frame = 0

        #we dont really need a high/low tick delay as the GPIO slows execution down way more that 1hz of 4mhz 
        if not sys.argv[1:]: time.sleep(ticklen)
        if physicalcpu == True: bottom.gpio = bottom.gpio - 1
        cpu.clock(False)

 
def exit_handler():#clean up of curses 
    os.system('stty sane')
    power.value = False
    logfile.close()
	def exit_handler():#clean up of curses
    os.system('stty sane')
    power.value = False  # turn off CPU
    logfile.close()
    # make sure Data bnd control us is set high, so Z80 doesn't get too hot driving low pins 
    d0.switch_to_output(value=True)
    d1.switch_to_output(value=True)
    d2.switch_to_output(value=True)
    d3.switch_to_output(value=True)
    d4.switch_to_output(value=True)
    d5.switch_to_output(value=True)
    d6.switch_to_output(value=True)
    d7.switch_to_output(value=True)
    clockpin.switch_to_output(value=True)
    reset.switch_to_output(value=True)
atexit.register(exit_handler)
